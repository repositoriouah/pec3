with Ada.Text_Io,Ada.Unchecked_Deallocation;
use Ada.Text_Io;

package body Colas is


  --Procedimiento para liberar memoria
  procedure Liberar is new Ada.Unchecked_Deallocation(Object=>Nodo, Name=>ref_Nodo);


  --Procedimiento que añade un elemento a la Cola
  procedure Poner (el_Elemento: Elementos; en_la_Cola: in out Cola) is
    elemento : ref_Nodo := new Nodo;
  begin
    elemento.Datos:= el_Elemento;
    if Esta_Vacia(en_la_Cola) then
      en_la_Cola.ptr_Primero:= elemento;
      en_la_Cola.ptr_Ultimo:= elemento;
      --en_la_Cola.ptr_Ultimo.ptr_Siguiente:= NULL;
    else
      en_la_Cola.ptr_Ultimo.ptr_Siguiente:= elemento;
      --en_la_Cola.ptr_Ultimo:= elemento;
      en_la_Cola.ptr_Ultimo:=en_la_Cola.ptr_Ultimo.ptr_Siguiente;
      en_la_Cola.ptr_Ultimo.ptr_Siguiente:= NULL;
    end if;
  end Poner;


  --Procedimiento Quitar un elemento, quitamos el primer elemento de la Cola
  --y devolvemos el elemento al programa principal
  procedure Quitar (un_Elemento: out Elementos; de_la_Cola: in out Cola) is
    primero : ref_Nodo;
  begin
    if Esta_Llena(de_la_Cola) then
      primero := de_la_Cola.ptr_Primero;
      un_Elemento:= primero.Datos;          --Elemento que devolvemos con la función quitar
      de_la_Cola.ptr_Primero:= de_la_Cola.ptr_Primero.ptr_Siguiente;
      Liberar(primero);
    else
      Put_Line("No hay elementos");
    end if;
  end Quitar;


  --Función que nos dice si una cola está vacía
  function Esta_Vacia (La_Cola: Cola) return Boolean is
  begin
    if La_Cola.ptr_Primero = NULL then
      return true;
    else
      return false;
    end if;
  end Esta_Vacia;


  --Función que nos dice si una cola tiene elementos
  function Esta_Llena (La_Cola: Cola) return Boolean is
  begin
    if not Esta_Vacia(La_cola) then
      return true;
    else
      return false;
    end if;
  end Esta_Llena;


  --Procedimiento que copia una Cola origen en una Cola destino
  procedure Copiar ( Origen: Cola; Destino:in out Cola) is
  begin
    Destino:=Origen;
  end Copiar;


  --Función que nos dice si dos Colas son iguales o si no son iguales
  function "="(La_Cola, Con_La_Cola: Cola) return Boolean is
    elem_cola_1, elem_cola_2: ref_Nodo;
  begin
    if Esta_Vacia(La_Cola) and Esta_Vacia(Con_La_Cola) then --Comprobamos que ambas esten vacias este vacía
      return true;
      else if (Esta_Llena(La_Cola) and Esta_Vacia(Con_La_Cola)) or (Esta_Vacia(La_Cola) and Esta_Llena(Con_La_Cola))  then
        return false;
        else
          elem_cola_1:= La_Cola.ptr_Primero;
          elem_cola_2:= Con_La_Cola.ptr_Primero;
          while (elem_cola_1 /= La_Cola.ptr_Ultimo) and (elem_cola_2 /= Con_La_Cola.ptr_Ultimo) loop
            if elem_cola_1.Datos=elem_cola_2.Datos then
                elem_cola_1:= elem_cola_1.ptr_Siguiente;
                elem_cola_2:= elem_cola_2.ptr_Siguiente;
            else
              return false;
            end if;
          end loop;
          return true;
      end if;
    end if;
  end "=";


end Colas;
